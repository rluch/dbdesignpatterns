import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Sandbox {
	public static void main(String[] args) { new Sandbox().go(); }
	
	public void go(){
		List<String> list = Arrays.asList("per", "ole", "ale", "Anders", "Peter", "Sebastian", "Bo");
		for(String s : list) System.out.println(s);
		System.out.println("=================");
		
		Collections.sort(list);
		for(String s : list) System.out.println(s);
		
		
		Collections.sort(list, new Comparator<String>(){

			@Override
			public int compare(String s1, String s2) {
				return s1.length() - s2.length();
			}
			
		});
		
		System.out.println("=================");
		for(String s : list) System.out.println(s);
	}
}
