package strategy;

public class Strategy_impl_B implements Strategy{

	@Override
	public String execute(int x) {
		return "B-"+x;
	}
}