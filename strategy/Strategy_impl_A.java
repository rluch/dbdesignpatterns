package strategy;

public class Strategy_impl_A implements Strategy{

	@Override
	public String execute(int x) {
		return "A-"+x;
	}
}