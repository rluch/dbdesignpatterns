package strategy;

public class Component_impl {
	private Strategy strategy;
	
	public Component_impl(Strategy strategy){
		this.strategy = strategy;
	}
	
	public String foo(int x){
		return this.strategy.execute(x);
	}
}