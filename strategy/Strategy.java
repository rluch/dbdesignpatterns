package strategy;

public interface Strategy {	
	public String execute(int x);
}