package strategy;

public class Test {

	public static void main(String[] args) {
		Component_impl c1 = new Component_impl(new Strategy_impl_A());
		System.out.println(c1.foo(4));
		
		Component_impl c2 = new Component_impl(new Strategy_impl_B());
		System.out.println(c2.foo(7));
	}
}