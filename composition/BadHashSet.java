package composition;

import java.util.Collection;
import java.util.HashSet;

public class BadHashSet<E> extends HashSet<E> {
	// HashSet is Serializable
	private static final long serialVersionUID = -2758557522025152945L;
	
	// The number of attempted element insertions
	private int addCount = 0;

	public BadHashSet() {
		super();
	}

	public BadHashSet(int initCap, float loadFactor) {
		super(initCap, loadFactor);
	}

	@Override
	public boolean add(E e) {
		this.addCount++;
		return super.add(e);
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		this.addCount += c.size();
		return super.addAll(c);
	}

	public int getAddCount() {
		return this.addCount;
	}
}
