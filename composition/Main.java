package composition;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		BadHashSet<String> s = new BadHashSet<>();
		s.addAll(Arrays.asList("Snap", "Crackle", "Pop"));
		
		System.out.println("AddCount = " + s.getAddCount());
		
		ForwardingHashSet<String> p = new ForwardingHashSet<>();
		p.addAll(Arrays.asList("Snap", "Crackle", "Pop"));
		
		System.out.println("AddCount = " + p.getAddCount());
	}
}
