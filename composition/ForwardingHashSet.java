package composition;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ForwardingHashSet<E> implements Set<E>{
	private HashSet<E> s;
	private int addCount = 0;
	
	public ForwardingHashSet(){
		this.s = new HashSet<>();
	}
	
	public ForwardingHashSet(int initCap, float loadFactor){
		this.s = new HashSet<>(initCap, loadFactor);
	}
	
	@Override public void clear(){this.s.clear();}
	@Override public boolean contains(Object o){return this.s.contains(o);}
	@Override public boolean isEmpty(){return this.s.isEmpty();}
	@Override public int size(){return this.s.size();}
	@Override public Iterator<E> iterator(){return this.s.iterator();}
	@Override public boolean add(E e){this.addCount++; return this.s.add(e);}
	@Override public boolean remove(Object o){return this.s.remove(o);}
	@Override public boolean containsAll(Collection<?> c){ return this.s.containsAll(c);}
	@Override public boolean addAll(Collection<? extends E> c){this.addCount+=c.size(); return this.s.addAll(c);}
	@Override public boolean removeAll(Collection<?> c){return this.s.removeAll(c);}
	@Override public boolean retainAll(Collection<?> c){return this.s.retainAll(c);}
	@Override public Object[] toArray(){return this.s.toArray();}
	@Override public <T> T[] toArray(T[] a){return this.s.toArray(a);}
	@Override public boolean equals(Object o){return this.s.equals(o);}
	@Override public int hashCode(){return this.s.hashCode();}
	@Override public String toString(){return this.s.toString();}
	
	public int getAddCount(){
		return this.addCount;
	}
}
