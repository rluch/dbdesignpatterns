package singleton;

public class Singleton {
	private static volatile Singleton instance = null;
	
	private Singleton(){}
	
	public static Singleton getInstance(){
		if(instance == null) {
			synchronized(Singleton.class){ //Double-check-locking
				if(instance == null) {
					instance = new Singleton(); //Lazy implementation
				}
			}
		}
		return instance;
	}
}