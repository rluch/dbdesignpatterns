package composite;

public interface Component {
	public void print();
}
