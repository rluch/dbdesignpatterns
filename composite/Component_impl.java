package composite;

public class Component_impl implements Component{
	private String name;
	
	public Component_impl(String name){
		this.name = name;
	}

	@Override
	public void print() {
		System.out.println(this.name);
	}
}