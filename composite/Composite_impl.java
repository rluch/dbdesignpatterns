package composite;

import java.util.ArrayList;

public class Composite_impl implements Component{
	private ArrayList<Component> list;
	
	public Composite_impl(){
		this.list = new ArrayList<>();
	}
	
	@Override
	public void print() {
		for(Component c : this.list){
			c.print();
		}
	}
	
	public void add(Component c){
		this.list.add(c);
	}
	
	public void remove(Component c){
		this.list.remove(c);
	}
}