package composite;

public class Test {

	public static void main(String[] args) {
		//Standard component
		Component c1 = new Component_impl("A");

		//Composite component
		Composite_impl c2 = new Composite_impl();
		for(int i=0; i<10; i++){
			c2.add(new Component_impl("B"+i));
		}

		c1.print();
		c2.print();
	}
}