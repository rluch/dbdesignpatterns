package command;

public class Command_impl_B implements Command{
	private int x;
	
	public Command_impl_B(int x){
		this.x = x;
	}
	
	@Override
	public void execute() {
		Test.sum += 10*this.x;
		
	}

	@Override
	public void undo() {
		Test.sum -= 10*this.x;
	}
}