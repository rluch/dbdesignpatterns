package command;

public class Command_impl_A implements Command{
	private int x = 0;

	public Command_impl_A(int x) {
		this.x = x;
	}

	@Override
	public void execute() {
		Test.sum += this.x;
	}

	@Override
	public void undo() {
		Test.sum -= this.x;
	}
}