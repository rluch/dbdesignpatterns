package command;

import java.util.Stack;

public class Test {
	public static int sum = 0;
	
	public static void main(String[] args) {
		Stack<Command> stack = new Stack<>();
			
		for(int i=0; i<2; i++){
			Command c = new Command_impl_A(3);
			stack.push(c);
			c.execute();
		}
		System.out.println("Sum = "+sum);
		
		for(int i=0; i<10; i++){
			Command c = new Command_impl_B(2);
			stack.push(c);
			c.execute();
		}
		System.out.println("Sum = "+sum);
		
		for(int i=0; i<5; i++){
			stack.pop().undo();
		}		
		System.out.println("Sum = "+sum);
	}
}