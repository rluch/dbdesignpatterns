package objectPool;

public class Pool {
	private Component_impl[] list;
	private boolean[] locks;
	
	public Pool(int size){
		this.list = new Component_impl[size];
		this.locks = new boolean[this.list.length];
		
		for(int i=0; i<this.list.length; i++){
			this.list[i] = new Component_impl("name");
			this.locks[i] = false;
		}	
	}
	
	public Component_impl getComponent(){
		for(int i=0; i<this.list.length; i++){
			Component_impl comp = this.list[i];
			if(!this.locks[i]){
				this.locks[i] = true;
				comp.setName("name"); //reset to original state
				return comp;
			}
		}
		return null;
	}
}
