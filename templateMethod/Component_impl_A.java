package templateMethod;

public class Component_impl_A extends Component_abs {

	@Override
	public int partialFoo(int y) {
		return y+1;
	}
}