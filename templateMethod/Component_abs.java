package templateMethod;

public abstract class Component_abs implements Component{
	
	@Override
	public String foo(String x, int y){
		x += "-";
		return x+partialFoo(y);
	}

	public abstract int partialFoo(int y);
}