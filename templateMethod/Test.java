package templateMethod;

public class Test {

	public static void main(String[] args) {
		Component c1 = new Component_impl_A();
		System.out.println(c1.foo("A", 2));
		
		
		
		//Ad hoc implementation
		Component c2 = new Component_abs() {
			@Override
			public int partialFoo(int y) {
				return y*2;
			}
		};
		System.out.println(c2.foo("B", 2));
	}
}