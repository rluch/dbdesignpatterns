package templateMethod;

public interface Component {
	public String foo(String x, int y);
}