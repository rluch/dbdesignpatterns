package decorator;

public class Decorator_impl_A extends Decorator{

	public Decorator_impl_A(Component component) {
		super(component);
	}
	
	@Override
	public void setName(String name){
		super.setName(name + "-A");
	}
}