package decorator;

public interface Component {
	public void setName(String name);
	public String getName();
}