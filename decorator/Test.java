package decorator;

public class Test {

	public static void main(String[] args) {
		Component c = new Decorator_impl_A(new Decorator_impl_A(new Component_impl()));
		c.setName("myName");
		
		System.out.println(c.getName());
	}
}