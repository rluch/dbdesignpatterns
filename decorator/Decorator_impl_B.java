package decorator;

public class Decorator_impl_B extends Decorator{

	public Decorator_impl_B(Component component) {
		super(component);
	}
	
	@Override
	public void setName(String name){
		super.setName(name+"-B");
	}
}