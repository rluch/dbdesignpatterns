package decorator;

public abstract class Decorator implements Component{
	protected Component component;
	
	public Decorator(Component component){
		this.component = component;
	}
	
	@Override
	public void setName(String name) {
		this.component.setName(name);
	}

	@Override
	public String getName() {
		return this.component.getName();
	}
}