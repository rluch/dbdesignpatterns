package decorator;

public class Component_impl implements Component{
	private String name;
	
	@Override
	public void setName(String name) { this.name = name; }

	@Override
	public String getName() { return this.name; }
}