package adaptor;

public class Component_impl_A implements ComponentA{
	private String name;
	
	public Component_impl_A(String name){
		this.name = name;
	}

	@Override
	public void printA() {
		System.out.println(this.name);
	}

	@Override
	public String getName() {
		return this.name;
	}
}