package adaptor;

public class Component_impl_B implements ComponentB{
	private String name;
	private int type;
	
	public Component_impl_B(String name, int type){
		this.name = name;
		this.type = type;
	}

	@Override
	public void printB() {
		System.out.println(this.name+"-"+this.type);
		
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public int getType() {
		return this.type;
	}
}