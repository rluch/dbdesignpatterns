package adaptor;

public interface ComponentB {
	public String getName();
	public int getType();
	
	public void printB();
}
