package adaptor;

public class Test {

	public static void main(String[] args) { 
		ComponentA a = new Component_impl_A("A-1");
		ComponentB b = new Component_impl_B("B", 1);
		
		a.printA();
		b.printB();
		
		ComponentB b2 = new AToB_Adaptor(a);
		b2.printB();
	}
}