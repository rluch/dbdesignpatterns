package adaptor;

public class AToB_Adaptor implements ComponentB{
	private String name;
	private int type;
	
	public AToB_Adaptor(ComponentA a){
		this.name = a.getName().split("-")[0].trim();
		this.type = Integer.parseInt(a.getName().split("-")[1].trim());
	}

	@Override
	public void printB() {
		System.out.println(this.name+"-"+this.type);
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public int getType() {
		return this.type;
	}
}