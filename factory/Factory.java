package factory;


public interface Factory {
	public Component createComponent(int type);
}