package factory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		Properties p = new Properties();
		String filename = "src\\factory\\props.txt";
		
		while(true){
			try { p.load(new FileInputStream(filename)); } 
			catch (IOException e) { e.printStackTrace(); }

			//Dependency injection
			char factoryType = p.getProperty("factory").charAt(0);
			int componentType = Integer.parseInt(p.getProperty("type"));
			
			Factory factory;
			switch(factoryType){
			case 'A': factory = new Factory_impl_A(); break;
			case 'B': factory = new Factory_impl_B(); break;
			default: factory = new Factory_impl_A();
			}
			
			Component comp = factory.createComponent(componentType);
			comp.print();
			
			//Repeat
			System.out.println("Press [ENTER] to repeat");
			Scanner scan = new Scanner(System.in);
			try{ scan.nextLine();}catch(Exception e){}
			scan.close();
		}
	}
}