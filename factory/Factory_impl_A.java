package factory;


public class Factory_impl_A implements Factory{

	@Override
	public Component createComponent(int type) {
		Component component;
		switch(type){
		case 1: component = new Component_impl_A1(); break;
		case 2: component = new Component_impl_A2(); break;
		default: component = null;
		}
		return component;
	}
}