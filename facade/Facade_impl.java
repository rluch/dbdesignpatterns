package facade;

public class Facade_impl {
	Component_impl_A a;
	Component_impl_B b;
	
	public Facade_impl(){
		this.a = new Component_impl_A();
		this.b = new Component_impl_B();
	}
	
	public void printFacade(){
		this.a.printA();
		this.b.printB();
	}
}