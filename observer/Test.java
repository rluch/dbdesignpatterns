package observer;

public class Test {
	public static void main(String[] args) {
		Component_impl c = new Component_impl("A");
		c.addObserver(new MyObserver());
		
		c.setName("B");
	}
}