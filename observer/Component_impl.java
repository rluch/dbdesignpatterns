package observer;

import java.awt.Color;
import java.util.Observable;

public class Component_impl extends Observable{
	private String name;

	public Component_impl(String name){
		setName(name);
	}

	public void setName(String name){

		this.name = name;
		setChanged();		// OBS



		notifyObservers(Color.RED);	// OBS
	}

	public String getName(){
		return this.name;
	}

	@Override
	public String toString(){
		return this.name;
	}
}