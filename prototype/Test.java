package prototype;

public class Test {
	
	public static void main(String[] args) {
		Component_impl prototype = new Component_impl();
		
		try {
			Component a = prototype.clone();
			a.setName("A");

			Component b = prototype.clone();
			b.setName("B");

			Component c = prototype.clone();
			c.setName("C");

			System.out.println(a);
			System.out.println(b);
			System.out.println(c);
		} catch (CloneNotSupportedException e) { /*Ignored*/ }
	}
}