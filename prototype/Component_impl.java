package prototype;

public class Component_impl implements Component, Cloneable{
	private String name;
	
	@Override
	public void setName(String name) { this.name = name; }

	@Override
	public String getName() { return this.name; }

	@Override
	public Component_impl clone() throws CloneNotSupportedException{
		return (Component_impl)super.clone();
	}
	
	@Override
	public String toString(){
		return this.name;
	}
}